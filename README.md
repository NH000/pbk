# pbk

## Description
pbk is a tool for backing up and retrieving files locally.

## Requirements

#### For installation
+ make
+ sh
+ sed
+ msgfmt
+ coreutils

#### For running
+ python3

## Installation
To install this program, enter its root directory and run `make install`.
That command will also install program assets into their appropriate directories.

You can also control installation options through `make` variables (which you can set on the command-line).
Run `make help` to list them.

## Uninstallation
To uninstall this program, enter its root directory and run `make uninstall`.
That command will remove installed program assets.

Note that installation options must be set to the values they possessed during the installation.

## Translations
Default language of this program (in effect when `C` or `POSIX` is set as locale) is US English.  
The following translations are available:

| **Language**       | **Translator**                                           | **For versions**                |
|--------------------|----------------------------------------------------------|---------------------------------|
| Serbian (Cyrillic) | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0                             |
| Serbian (Latin)    | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0                             |

### Translation process
This program is written to be easily translatable into multiple languages, and it achieves that through the use of [`gettext`](https://docs.python.org/3/library/gettext.html) package.
Translations are located in `po` directory. Files in that directory contain translations, each PO file corresponding to one locale.

To add a new or update existing translation, enter the project's `po` directory and run the following command:

```
make %.po  # Generate/update PO file; "%" should be replaced with a language code.
```

Afterwards, you can edit created/updated PO file (see [`gettext` manual](https://www.gnu.org/software/gettext/manual/gettext.html) for details),
translating the program that way.

You could also run `make messages.pot` to just generate the template file, but this will be done automatically by the previously described rule.

Also, it would be good to translate the manual page; you will find it in `man` project subdirectory.
