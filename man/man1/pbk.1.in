.\" Man page for PBK
.TH "PBK" 1 "4 May 2021" "1.0" "Man page for PBK"
.SH NAME
PBK \- back up and retrieve files locally
.SH SYNOPSIS
.B PBK_EXEC_NAME
.RB [ -h ]
.RB [ -v ]
.RB [ -l ]
.RB [ -b
.I FILE
.RI [ FILE " ...]"\~
.RB [ -r
.RI [ FILE " ...]]"\~
.RB [ -u ]
.RB [ -d
.RI [ FILE " ...]]"\~
.RB [ --tree ]
.RB [ --no-symlinks ]
.RB [ --retrieve-dir
.IR DIRECTORY ]
.RB [ --no-headers ]
.SH DESCRIPTION
This program's main functions are copying files to and from specific directory.
This specific directory is known as the "backup directory".
Copying files to the backup directory is called "backing files up",
and copying files from the backup directory is called "retrieving files".
.P
Outside of backing up and retrieving files, this program can also list backup directory's contents and delete backed up files.
Together, listing backed up files, backing up files, retrieving backed up files and deleting backed up files are known as "actions".
See
.B ACTIONS
section to get information on each specific action and their execution order when more than one is specified.
Also, at least one action must be passed to the program unless help menu or program version is requested.
.P
Backup directory is determined by the environment variable
.BR PBK_BACKUP_DIR ,
hence see the
.B ENVIRONMENT
section for information on it.
.SH OPTIONS
Unless help menu or program version is requested, you must supply at least one of the action options.
See
.B ACTIONS
section to get information on actions and their associated options.
.TP
.B -h, --help
Display help menu and exit.
.TP
.B -v, --version
Display program version and exit.
.TP
.B -l, --list
List backed up files (files in the backup directory).
.IP
By default, only the top-level backup directory's contents are listed in the basic print style, you can pass
.B --tree
along with this option to recursively print backup directory's contents as a tree structure.
.TP
.B "-b"\c
.IR "FILE" " [" "FILE" " ...], "\c
.B "--backup"\c
.IR "FILE" " [" "FILE" " ...]"
Copy specified files to the backup directory.
Backup directory will be created if it does not exist, along with all its parent directories.
.IP
.I FILE
can designate any file, except for the backup directory itself or one of its subfiles, or one of its parent directories.
.IP
Symlinks are followed by default when determining which files to copy (i.e. "backup"), you can pass
.B --no-symlinks
to disable this behavior.
.TP
.B "-r"\c
.RI "[" "FILE" " ...], "\c
.B "--retrieve"\c
.RI "[" "FILE" " ...]"
Copy specified files from the backup directory to the current directory if
.B --retrieve-dir
is not specified, and to the directory specified by
.B --retrieve-dir
if it is.
If retrieval directory does not exist, it will be created, along with all its parent directories.
.IP
.I FILE
can designate any file in the backup directory, on any directory level, when it is appended to the backup directory path.
In case that no files are specified, all backed up files are copied (i.e. "retrieved").
.IP
File is not retrieved if some file with its name already exists in the retrieval directory.
You can pass
.B -u
to change this behavior, in which case the existing file in the retrieval directory will be replaced.
.TP
.B -u, --update
Replace existing files when retrieving backed up files to the retrieval directory.
.TP
.B "-d"\c
.RI "[" "FILE" " ...], "\c
.B "--delete"\c
.RI "[" "FILE" " ...]"
Delete specified files from the backup directory.
.I FILE
must name a file in the backup directory, on any directory level, after it is appended to the backup directory.
If no files are specified, all backup directory's contents are deleted.
.TP
.B --tree
When
.B -l
is specified, recursively list backup directory's contents as a tree structure (similar to
.BR tree (1)).
.TP
.B --no-symlinks
When
.B -b
is specified, do not follow symlinks when copying files specified by it.
.TP
.BI --retrieve-dir " DIRECTORY"
When
.B -r
is specified, copy files to
.I DIRECTORY
instead of to the current directory.
.I DIRECTORY
must not be the backup directory itself or one of its subdirectories.
.TP
.B --no-headers
Do not print action headers.
.SH ENVIRONMENT
All environment variables private to this program start with "PBK_".
.TP
.B PBK_BACKUP_DIR
Designates the path to the backup directory.
.IP
If this environment variable is not defined, backup directory defaults to (in
.BR bash (1)
script) "${XDG_DATA_HOME-${HOME-~}/.share}/pbk".
.SH ACTIONS
For this program to do something, you must specify to it one or more actions.
Actions are specified through command-line options (described in
.B OPTIONS
section).
In this section, all actions are described in the order of their execution (which is useful to know if multiple actions are specified),
and description for each action also includes which command-line options control that action.
.SS Retrieving backed up files
The first action to be executed is the retrieval of backed up files.
This action is requested by specifying
.B -r
command-line argument.
.P
When this action is executed, the program will copy files specified by
.BR -r ,
appended to the backup directory path, which must be in the backup directory on any directory level, to the directory specified by
.BR --retrieve-dir ,
if that argument is specified, or to the current directory, if it is not.
If no files are given to
.BR -r ,
all backed up files are retrieved.
.P
If file which has the name of a file to retrieve already exists in the retrieval directory, then file to retrieve is not copied by default,
but if
.B -u
is passed, existing file is replaced.
.P
Retrieval directory must not be the backup directory itself or one of its subdirectories, and it will be created if it does not exist,
along with all its parent directories.
.SS Deleting backed up files
The second action to be executed is the deletion of backed up files.
This action is requested by specifying
.B -d
command-line argument.
.P
When this action is executed, the program will delete files specified by
.BR -d ,
appended to the backup directory path, which must be in the backup directory on any directory level.
If no files are given to
.BR -d ,
all backed up files are deleted.
.SS Backing up files
The third action to be executed is the backing up of files.
This action is requested by specifying
.B -b
command-line argument.
.P
When this action is executed, the files specified on the command-line are copied to the backup directory.
No file given to
.B -b
can be the backup directory itself or one of its subfiles, or one of its parent directories.
.P
By default, symlinks are followed when determining which files to backup.
You can suppress this behavior by passing
.BR --no-symlinks .
.P
Backup directory will be created if it does not exist, along with all its parent directories.
.SS Listing backed up files
The fourth action to be executed is the listing of backed up files.
This action is requested by specifying
.B -l
command-line argument.
.P
When this action is executed, the program prints backed up files i.e. backup directory's contents.
This printing can be done in two styles:
.RS
.IP \(bu
Default style is just printing of the top-level backup directory's contents, where each file is separated by a newline.
.IP \(bu
If
.B --tree
is passed, backup directory's contents are printed recursively as a tree structure
(similar to
.BR tree (1)).
.RE
.SH NOTES
All paths received externally will be normalized.
This includes backup directory, as well as files supplied on the command-line.
.P
File mode and status are not copied when backing up or retrieving files.
.SH RETURN VALUE
.TS
r l.
0	No errors.
1	Invalid command-line arguments.
2	Runtime error.
.TE
.SH BUGS
When copying a directory, the program may overwrite existing directory (not replace it), or refuse to do the copying.
This can happen because the directory copying operation is executed in three steps:
.RS
.nr step 1 1
.IP \n[step].
If
.B -u
is specified, existence of a file under the target filename is tested and the file is deleted if it exists.
.IP \n+[step].
Existence of a file under the target filename is tested.
This is done because the program should not overwrite existing directory if
.B -u
is not given, and because it is a little safer to do the check for the existence of a file under the target filename again, even if the first step was executed.
.IP \n+[step].
The source directory is copied.
.RE
.sp
We can see that the refusal to do the copying may come from the race condition between the first and the second step, or from the race condition between the second
and third step if non-directory file was created.
Directory overwiting may happen because of the race condition between the second and thrid step if directory was created.
.P
When copying a file which is not a directory, the program may overwrite existing file if
.B -u
is not given, due to the race condition between the check for the existence of a file under the target filename and the copying of the source file.
.SH SEE ALSO
.BR python (1)
