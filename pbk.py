#!/bin/env python3

# Copyright (C) 2021 Nikola Hadžić
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the ""Software""), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import gettext
import sys
import os
import errno
import stat
import argparse
import shutil

# Text domain setup.
TEXTDOMAINDIR = None
TEXTDOMAIN = None

# Prepare internationalization.
_ = gettext.gettext
gettext.bindtextdomain(TEXTDOMAIN, TEXTDOMAINDIR)
gettext.textdomain(TEXTDOMAIN)

# Set program name.
PROG = os.path.basename(sys.argv[0])

# Get environment variables.
ENV_PBK_BACKUP_DIR = os.path.normpath(os.getenv("PBK_BACKUP_DIR", os.path.join(os.getenv("XDG_DATA_HOME", os.path.join(os.getenv("HOME", os.path.expanduser("~")), ".share")), "pbk")))

# Returns whether the supplied path is a directory.
# On error an exception is raised.
def check_is_dir(path):
    return stat.S_ISDIR(os.stat(path).st_mode)

# Returns directory contents as a list.
# Directories will have "/" appended.
# Empty list is returned if `directory` does not exist.
# Exception is raised on error.
def get_dir_contents(directory):
    # Retrieve directory contents.
    try:
        directory_contents = os.listdir(directory)
    except OSError as error:
        if error.errno == errno.ENOENT:
            directory_contents = []
        else:
            raise

    # Determine whether each returned file is a directory.
    for i in range(len(directory_contents) - 1, -1, -1):
        try:
            if check_is_dir(os.path.join(directory, directory_contents[i])):
                directory_contents[i] = directory_contents[i] + "/"
        except OSError:
            del directory_contents[i]

    return directory_contents

# Returns whether two given filenames have the same path.
def is_file_same(file1, file2):
    return os.path.realpath(file1) == os.path.realpath(file2)

# Returns whether given file's path is a subpath of the given directory's path.
def is_file_inside(file, directory):
    return not is_file_same(file, directory) and os.path.realpath(file).startswith(os.path.realpath(directory))

# Validates user-supplied file list by removing the
# unfitting elements.
# After removing all the invalid elements from the
# file list, the modified list is returned.
# `policy` determines validation policy, allowed values
# are: "general", "individual".
def validate_user_file_list(files, directory, policy = "general", *, directory_dest = None, silent = False):
    # Iteratively test each supplied file for validity.
    files = files[::-1]    # Reverse file list so that files are validated in the original order when going in reverse.
    for i in range(len(files) - 1, -1, -1):
        # Normalize file's path.
        files[i] = os.path.normpath(files[i])

        # Get full file path.
        path = os.path.join(directory, files[i])

        # Validity checks.
        if policy == "general" and not is_file_inside(path, directory):
            if not silent:
                print(_("File '%(file)s' is not inside of '%(dir)s'") % {"file" : files[i], "dir" : directory}, file = sys.stderr)
            del files[i]
        elif policy == "individual" and (is_file_same(path, directory_dest) or is_file_inside(path, directory_dest) or is_file_inside(directory_dest, path)):
            if not silent:
                print(_("File '%(file)s' either is, is in, or contains '%(dest)s'") % {"file" : files[i], "dest" : directory_dest}, file = sys.stderr)
            del files[i]
        else:
            # Determine file's type.
            try:
                if check_is_dir(path):
                    files[i] = files[i] + "/"
            except OSError as error:
                if not silent:
                    print(_("Could not determine type of '%(file)s': %(msg)s") % {"file" : files[i], "msg" : error.strerror}, file = sys.stderr)
                del files[i]

    # Restore the original order and return cleared file list.
    files = files[::-1]
    return files

# Deletes file or a directory.
# Directories should and in "/".
# On error an exception is raised.
def delete_file(file):
    if file[-1] == "/":
        shutil.rmtree(file)
    else:
        os.remove(file)

# Copies source to destination file.
# If source file is a directory, it should end in "/".
def copy_file(file_src, file_dest, follow_symlinks = False, update = True):
    # Remove existing file if overwriting is allowed.
    # This is only needed if `file_src` is a directory.
    # FIXME: There is a race condition between the deletion of the existing file and the copying of the source directory.
    if update and file_src[-1] == "/":
        try:
            is_dir = check_is_dir(file_dest)
        except OSError:
            pass
        else:
            try:
                delete_file(file_dest + ("/" if file_dest != "/" and is_dir else ""))
            except OSError:
                pass

    # Copy file according to its type.
    if file_src[-1] == "/" and not os.path.exists(file_dest):
        shutil.copytree(file_src, file_dest, symlinks = not follow_symlinks, copy_function = shutil.copyfile, ignore_dangling_symlinks = True, dirs_exist_ok = True)    # FIXME: Because the destination deletion and the source copying are separated, it is possible to attempt to copy into a file created between the two calls.
    elif update or not os.path.exists(file_dest):   # FIXME: There is a race condition between the check for the destination file's existence and the copying of the source file. Thus this operation may overwrite a file created between the two calls.
        shutil.copyfile(file_src, file_dest, follow_symlinks = follow_symlinks)
    else:
        raise FileExistsError(errno.EEXIST, _("File exists"))

# Parses command-line arguments and returns structure that contains parsed data.
def parse_args(argv):
    # Create parser object.
    parser = argparse.ArgumentParser(prog = PROG, description = _("Back up and retrieve files locally."), epilog = _("See pbk(1) for more information."), add_help = True)

    # Add command-line arguments to scan for.
    parser.add_argument("-v", "--version", action = "version", help = _("display program version and exit"), version = "1.0")
    parser.add_argument("-l", "--list", action = "store_true", default = False, help = _("list backed up files"))
    parser.add_argument("-b", "--backup", nargs = "+", help = _("specify files to back up"), metavar = _("FILE"))
    parser.add_argument("-r", "--retrieve", nargs = "*", help = _("specify backed up files to retrieve"), metavar = _("FILE"))
    parser.add_argument("-u", "--update", action = "store_true", default = False, help = _("replace existing files when retrieving"))
    parser.add_argument("-d", "--delete", nargs = "*", help = _("specify backed up files to delete"), metavar = _("FILE"))
    parser.add_argument("--tree", action = "store_true", default = False, help = _("list backed up files as a tree structure, recursively"))
    parser.add_argument("--no-symlinks", action = "store_true", default = False, help = _("do not follow symlinks when backing up files"))
    parser.add_argument("--retrieve-dir", help = _("directory to retrieve files to"), metavar = _("DIRECTORY"))
    parser.add_argument("--no-headers", action = "store_true", default = False, help = _("do not print action headers"))

    # Parse command-line arguments.
    arguments = parser.parse_args(argv)

    # Check for argument validity.
    if not (arguments.list or arguments.backup != None or arguments.retrieve != None or arguments.delete != None):
        parser.exit(_("%(prog)s: error: at least one action must be specified") % {"prog" : PROG})
    if arguments.update and arguments.retrieve == None:
        parser.exit(_("%(prog)s: error: -u/--update used without -r/--retrieve") % {"prog" : PROG})
    if arguments.tree and not arguments.list:
        parser.exit(_("%(prog)s: error: --tree used without -l/--list") % {"prog" : PROG})
    if arguments.no_symlinks and arguments.backup == None:
        parser.exit(_("%(prog)s: error: --no-symlinks used without -b/--backup") % {"prog" : PROG})
    if arguments.retrieve_dir and arguments.retrieve == None:
        parser.exit(_("%(prog)s: error: --retrieve-dir used without -r/--retrieve") % {"prog" : PROG})

    return arguments

# Lists directory contents in basic style.
def list_files(directory):
    # Normalize directory path.
    directory = os.path.normpath(directory)

    # Get directory contents.
    try:
        directory_contents = get_dir_contents(directory)
    except OSError as error:
        print(_("Could not get contents of '%(dir)s': %(msg)s") % {"dir" : directory, "msg" : error.strerror}, file = sys.stderr)
        exit(2)

    # Print directory's files, one after another.
    for file in directory_contents:
        print(file)

# Lists directory contents as a tree structure.
# `last_in_parents` holds boolean values that say,
# from first to last containing parent directory,
# whether this is or is contained in the last
# directory in it. This information is used to
# properly draw tree structure.
def list_files_tree(directory, last_in_parents = []):
    # Normalize directory path.
    directory = os.path.normpath(directory)

    # Get directory contents.
    try:
        directory_contents = get_dir_contents(directory)
    except OSError as error:
        if len(last_in_parents) == 0:
            print(_("Could not get contents of '%(dir)s': %(msg)s") % {"dir" : directory, "msg" : error.strerror}, file = sys.stderr)
            exit(2)

    # Print root directory.
    if len(last_in_parents) == 0 and len(directory_contents) != 0:
        print(directory, end = "/\n" if directory[-1] != "/" else "\n")

    # Print directory's files as a tree structure.
    for i, file in enumerate(directory_contents):
        # Print tree structure of the parent directories.
        for last_in_parent in last_in_parents:
            print(" " if last_in_parent else "\u2502", end = "")
            print("   ", end = "")

        # Get whether this is the last file in this directory.
        is_last = len(directory_contents) - 1 == i

        # Print current file.
        print("\u2514" if is_last else "\u251c", end = "")
        print("\u2500\u2500 ", end = "")
        print(file)

        # If this is a directory, print its contents too.
        if file[-1] == "/":
            list_files_tree(os.path.join(directory, file), last_in_parents + [is_last])

# Delete files in `directory`, specified by `files`.
# `directory` and `files` will be appended and the
# result must be in `directory`.
# If `files` is None, all `directory` contents are
# deleted.
def delete_files(directory, files = None):
    # Normalize directory path.
    directory = os.path.normpath(directory)

    # If `files` is None, get directory contents.
    if files == None:
        try:
            files = get_dir_contents(directory)
        except OSError as error:
            print(_("Could not get contents of '%(dir)s': %(msg)s") % {"dir" : directory, "msg" : error.strerror}, file = sys.stderr)
            exit(2)
    else:   # Otherwise, determine validity and types of the supplied files.
        files = validate_user_file_list(files, directory)

    # Delete supplied or retrieved files from the directory.
    for file in files:
        # Get full file path.
        path = os.path.join(directory, file)

        # Attempt the deletion.
        try:
            delete_file(path)
        except OSError as error:
            print(_("Could not delete '%(file)s': %(msg)s") % {"file" : file, "msg" : error.strerror}, file = sys.stderr)
        else:
            print(_("Deleted '%(file)s'") % {"file" : file})

# Copies specified files from the source to the destination
# directory.
# If `files` is None, all files contained in the source
# directory are copied.
# If `follow_symlinks` is true, target file of a symlink source
# file is copied instead of the symlink itself, and vice versa.
# If `update` is true, existing file is replaced on copying,
# otherwise source file is not copied.
# In some situations different copying policies are needed, and
# `policy` parameter controls which policy to use. Allowed
# values are: "general", "individual".
def copy_files(directory_src, directory_dest, files = None, follow_symlinks = False, update = True, policy = "general"):
    # Normalize directory paths.
    directory_src = os.path.normpath(directory_src)
    directory_dest = os.path.normpath(directory_dest)

    if policy == "general":
        # Destination directory must not be a subdirectory of the source directory.
        if is_file_same(directory_src, directory_dest) or is_file_inside(directory_dest, directory_src):
            print(_("Destination directory '%(dest)s' is or is a subdirectory of source directory '%(src)s'") % {"dest" : directory_dest, "src" : directory_src}, file = sys.stderr)
            exit(2)

    # If `files` is None, get all source directory files for copying.
    if files == None:
        try:
            files = get_dir_contents(directory_src)
        except OSError as error:
            print(_("Could not get contents of '%(dir)s': %(msg)s") % {"dir" : directory_src, "msg" : error.strerror}, file = sys.stderr)
            exit(2)
    else:   # If not, determine validity and types of the supplied files.
        files = validate_user_file_list(files, directory_src, policy, directory_dest = directory_dest if policy == "individual" else None)

    # Create destination directory in case that it does not exist.
    if len(files) != 0:
        os.makedirs(directory_dest, mode = 0o755, exist_ok = True)

    # Attempt to copy each file from the source to the destination directory.
    for file in files:
        # Get full paths for the file to be copied and its new location.
        path_src = os.path.join(directory_src, file)
        path_dest = os.path.join(directory_dest, os.path.basename(os.path.normpath(path_src)))

        # Copy the source file to the destination location.
        # Make sure that if `update` is false, file under that name does not exist already.
        try:
            copy_file(path_src, path_dest, follow_symlinks, update)
        except OSError as error:
            if isinstance(error.args[0], list): # If this is true, then there was an error while copying multiple files, which means that the current file is a directory and there were problems copying its subfiles.
                print(_("Not all files of '%(dir)s' could be copied") % {"dir" : file}, file = sys.stderr)
            else:
                print(_("Could not copy '%(file)s': %(msg)s") % {"file" : file, "msg" : error.strerror}, file = sys.stderr)
        else:
            print(_("Copied '%(file)s'") % {"file" : file})

### BEGINNING OF THE PROGRAM ###
###==========================###

# Parse command-line arguments.
arguments = parse_args(sys.argv[1:])

# Header printer.
header = lambda head : print("===" + head + "===")

# Execute the requested actions.
if arguments.retrieve != None:
    if not arguments.no_headers:
        header(_("RETRIEVAL"))
    copy_files(ENV_PBK_BACKUP_DIR, arguments.retrieve_dir if arguments.retrieve_dir else os.getcwd(), arguments.retrieve if arguments.retrieve else None, update = arguments.update)
if arguments.delete != None:
    if not arguments.no_headers:
        header(_("DELETION"))
    delete_files(ENV_PBK_BACKUP_DIR, arguments.delete if arguments.delete else None)
if arguments.backup != None:
    if not arguments.no_headers:
        header(_("BACKING UP"))
    copy_files(os.getcwd(), ENV_PBK_BACKUP_DIR, arguments.backup, not arguments.no_symlinks, policy = "individual")
if arguments.list:
    if not arguments.no_headers:
        header(_("LISTING"))
    if arguments.tree:
        list_files_tree(ENV_PBK_BACKUP_DIR)
    else:
        list_files(ENV_PBK_BACKUP_DIR)
